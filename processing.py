import pandas as pd
import re


def merge_dataframes_by_truck_id(df_maintenance, df_fleet):
    """
    Merges two DataFrames based on the 'truck_id' column.

    Parameters:
    - df_maintenance (pandas DataFrame): The DataFrame containing maintenance data.
    - df_fleet (pandas DataFrame): The DataFrame containing fleet data.

    Returns:
    - pandas DataFrame: The merged DataFrame.

    Side Effects:
    - None

    Assumptions:
    - Both DataFrames contain a column named 'truck_id'.
    """
    merged_dataframe = pd.merge(df_maintenance, df_fleet, on='truck_id', how='inner')
    return merged_dataframe

def standardize_service_type(dataframe):
    """
    Standardizes the values in the 'service_type' column to lowercase and removes leading/trailing whitespace.

    Parameters:
    - dataframe (pandas DataFrame): The DataFrame to be standardized.

    Returns:
    - None

    Side Effects:
    - Modifies the 'service_type' column in the given DataFrame to lowercase and removes leading/trailing whitespace.

    Assumptions:
    - The DataFrame contains a column named 'service_type'.
    """
    dataframe['service_type'] = dataframe['service_type'].str.lower().str.strip()

def one_hot_encode_service_type(dataframe):
    """
    Performs one-hot encoding on the 'service_type' column.

    Parameters:
    - dataframe (pandas DataFrame): The DataFrame to be one-hot encoded.

    Returns:
    - pandas DataFrame: The DataFrame with one-hot encoded columns.

    Side Effects:
    - None

    Assumptions:
    - The DataFrame contains a column named 'service_type'.
    """
    dataframe = pd.get_dummies(dataframe, columns=['service_type'])
    return dataframe

def check_and_add_valid_email_column(dataframe):
    """
    Checks the validity of email addresses in the 'technician_email' column and adds a new column 'valid_email' indicating True or False.

    Parameters:
    - dataframe (pandas DataFrame): The DataFrame containing the 'technician_email' column.

    Returns:
    - None

    Side Effects:
    - Adds a new column 'valid_email' to the DataFrame indicating whether each email address is valid or not.

    Assumptions:
    - The DataFrame contains a column named 'technician_email'.
    """
    regex = r'^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w+$'
    valid_email = []
    for email in dataframe['technician_email']:
        email = str(email)
        valid_email.append(bool(re.fullmatch(regex, email)))
    dataframe['valid_email'] = valid_email

def adjust_date_format(dataframe):
    """
    Adjusts the date format in the input DataFrame to 'dd-mm-yyyy'.

    Parameters:
    - dataframe (pandas DataFrame): The input DataFrame containing date columns to be adjusted.

    Return Value:
    None

    Side Effects:
    This function modifies the date format of the 'purchase_date' or 'maintenance_date' columns in the DataFrame
    to 'dd-mm-yyyy' format.

    Assumptions:
    - The input DataFrame may contain either a 'purchase_date' column or a 'maintenance_date' column or both.

    """
    
    if 'purchase_date' in dataframe.keys():
        dataframe['purchase_date'] = pd.to_datetime(dataframe['purchase_date'], format='%d-%m-%Y', errors='coerce')
        dataframe['purchase_date'] = dataframe['purchase_date'].dt.strftime("%d-%m-%Y")
        
    if 'maintenance_date' in dataframe.keys():
        dataframe['maintenance_date'] = pd.to_datetime(dataframe['maintenance_date'], format='%d-%m-%Y', errors='coerce')
        dataframe['maintenance_date'] = dataframe['maintenance_date'].dt.strftime("%d-%m-%Y")