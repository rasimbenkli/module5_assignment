# importing necessary libraries
import pandas as pd
import json
from Loggers_file import application_logger

def read_configuration_file():
    """
    Reads the configuration file 'data_sources.json' to retrieve information about dataset paths.

    Returns:
    dict: A dictionary containing information from the configuration file.

    Side Effects:
    - Reads the 'data_sources.json' file from the specified path.
    - Logs an error if the configuration file cannot be read.

    Raises:
    Exception: If there is an error reading the configuration file.
    """
    try:
        with open("C:/Users/RBE95Y/Desktop/Module5_Assignment/data_sources.json", "r") as config_file:
            global config_info
            config_info = json.load(config_file)

        return config_info

    except Exception as e:
        application_logger.error('Cannot read configuration file data_sources.json', exc_info=True)
        raise

def read_maintenance_dataset():
    """
    Reads the maintenance dataset into a DataFrame.

    Returns:
    pandas DataFrame: The DataFrame containing the maintenance dataset.

    Side Effects:
    - Reads the maintenance dataset from the file specified in the configuration.
    - Logs an error if there is an issue reading the dataset.

    Raises:
    Exception: If there is an error reading the maintenance dataset.
    """
    try:
        for file_info in config_info['data_sources']:
            file_path = file_info['path']
            
            if file_info['type'] == 'maintenance':
                dataframe_maintenance = pd.read_csv(file_path)
                return dataframe_maintenance
            
    except Exception as e:
        application_logger.error('Error when creating the maintenance dataframe', exc_info=True)
        raise

def read_fleet_dataset():
    """
    Reads the fleet dataset into a DataFrame.

    Returns:
    pandas DataFrame: The DataFrame containing the fleet dataset.

    Side Effects:
    - Reads the fleet dataset from the file specified in the configuration.
    - Logs an error if there is an issue reading the dataset.

    Raises:
    Exception: If there is an error reading the fleet dataset.
    """
    try:
        for file_info in config_info['data_sources']:
            file_path = file_info['path']

            if file_info['type'] == 'fleet':
                dataframe_fleet = pd.read_csv(file_path)
                return dataframe_fleet
            
    except Exception as e:
        application_logger.error('Error when creating the fleet dataframe', exc_info=True)
        raise






