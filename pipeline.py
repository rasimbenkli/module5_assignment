from data_read import read_configuration_file,read_maintenance_dataset,read_fleet_dataset
from Loggers_file import application_logger
from quality_checks import count_missing_values_per_column,find_unique_truck_ids,count_duplicate_rows
from processing import merge_dataframes_by_truck_id,standardize_service_type,one_hot_encode_service_type,check_and_add_valid_email_column,adjust_date_format


if __name__ == "__main__":

    # 1 - READING JSON CONFIGURATION FILE
    try:
        config_info = read_configuration_file()
        
    except Exception as e:
        application_logger.error(f'PIPELINE--Reading configuration file failed! PIPELINE terminated!: {e}\n')
        raise SystemExit(1)
    

    # 2 - LOADING MAINTENANCE AND FLEET DATASETS
    ## Maintenance
    try:
        maintenance_df = read_maintenance_dataset()
    except Exception as e:
        application_logger.error(f'PIPELINE--Reading maintenance_df into DataFrame failed! PIPELINE terminated! {e}\n')
        raise SystemExit(1)
    
    ## Fleet
    try:
        fleet_df = read_fleet_dataset()
    except Exception as e:
        application_logger.error(f'PIPELINE--Reading fleet_df into DataFrame failed! PIPELINE terminated! {e}\n')
        raise SystemExit(1)
    


    # 3 - CHECKS AND REMOVES DUPLICATE RECORDS IF EXIST
    ## Maintenance
    try:
        maintenance_duplicate_count = count_duplicate_rows(maintenance_df)
        if maintenance_duplicate_count > 0:
            maintenance_df = maintenance_df.drop_duplicates()
    except Exception as e:
        application_logger.error(f'PIPELINE--Error while removing duplicates from maintenance dataframe {e}\n')
    
    # Fleet
    try:
        fleet_duplicates_count = count_duplicate_rows(fleet_df)
        if fleet_duplicates_count > 0:
            fleet_df = fleet_df.drop_duplicates()
    except Exception as e:
        application_logger.error(f'PIPELINE--Error while removing duplicates from fleet dataframe {e}\n')


    # 4 - ANALYSIS TO IDENTIFY WITH MISSING DATA
    ## Maintenance
    try:
        count_missing_values_per_column(maintenance_df)
    except Exception as e:
        application_logger.error(f'PIPELINE--Error while calling count_missing_values_per_column() function for maintenance_df {e}\n')

    ## Fleet
    try:
        count_missing_values_per_column(fleet_df)
    except Exception as e:
        application_logger.error(f'PIPELINE--Error while calling count_missing_values_per_column() function for fleet_df {e}\n')


    # 5 - IDENTIFY TRUCK IDs PRESENT IN THE MAINTENANCE_DF THE MISSING FROM THE FLEET_DF
    try:
        find_unique_truck_ids(maintenance_df, fleet_df)
    except Exception as e:
        application_logger.error(f'PIPELINE--Error while calling find_unique_truck_ids() function. {e}\n')

    # 6 - CONVERTING DATAE COLUMNS TO %d-%m-%Y FORMAT
    ## Maintenance
    try: 
        adjust_date_format(maintenance_df)
    except Exception as e:
        application_logger.error(f'PIPELINE--Error while calling date_format_adjuster() function for maintenance_df. {e}\n')

    ## Fleet
    try: 
        adjust_date_format(fleet_df)
    except Exception as e:
        application_logger.error(f'PIPELINE--Error while calling date_format_adjuster() function for fleet_df. {e}\n')

    
    # 7 - STANDARDIZING  THE SERVICE_TYPE COLUMN
    try: 
        standardize_service_type(maintenance_df)
    except Exception as e:
        application_logger.error(f'PIPELINE--Error while calling standardizer() function {e}\n')


    # 8 - APPLYING ONE-HOT-ENCODING TO SERVICE_TYPE COLUMN
    try:
        maintenance_df = one_hot_encode_service_type(maintenance_df)
    except Exception as e:
        application_logger.error(f'PIPELINE--Error while calling one_hot_encoding() function {e}\n')

    # 9 - VALIDATING THE FORMAT OF TECHNICIAN EMAIL ADDRESSES BY USING REGEX AND CREATING A NEW COLUMN CALLED EMAIL_VALID
    try:
        check_and_add_valid_email_column(maintenance_df)
    except Exception as e:
        application_logger.error(f'PIPELINE--Error while calling valid_email() function {e}\n')

    # 10 - MERGING THE FLEET AND THE MAINTENANCE DATASETS ON TRUCK_ID COLUMN
    try:
        merged_datasets = merge_dataframes_by_truck_id(maintenance_df, fleet_df)
    except Exception as e:
        application_logger.error(f'PIPELINE--Error while calling mergind_dataframes() function {e}\n')
    
    # 11 - SAVING MERGED DATASET INTO A CSV FILE

    try: 
        merged_datasets.to_csv('fleet_maintenance_MERGED.csv', index=False, mode='w')
    except Exception as e:
        application_logger.error(f'PIPELINE--Error while saving merged datasets to CSV file {e}\n')
    






    


