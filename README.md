# Data Analysis Pipeline for Maintenance and Fleet Truck Datasets

The primary objective of this project is to develop a robust data analysis pipeline tailored to both maintenance and fleet datasets, which collectively hold information about trucks. The pipeline aims to integrate maintenance data with fleet information, enabling detailed insights into the maintenance both datasets.

## Setup and Installation

* Clone the repository to you local machine
* Run the 'pipeline.py' using a command shell or an IDE
* Version: Python 3.11.5**

## Dependencies

The project has the following dependencies:

* You might need to update or install the libraries below.
1) Pandas -- pip install pandas
2) Numpy  -- pip install numpy



## Files

1) **data_sources.json:** This file serves as the configuration file, containing the paths to the datasets used in the project.
2) **application_log:** This directory contains logs related to application errors, it is set to be re-written.
3) **data_quality_log:** Logs for the data quality checks are stored in this directory, it is set to be re-written.
4) **data_read.py:** This file reads the data sources paths from data_sources.json
5) **quality_checks.py:** This module is dedicated to checking the quality of the data and logging any relevant information regarding data quality.
6) **processing.py:** Here, the data transformation tasks are carried out according to the project requirements.
7) **pipeline.py:** Serving as the main class to initiate the data processing pipeline, this file orchestrates the entire data processing workflow.
8) **fleet_information.csv** Dataset that holds the fleet information.
9) **maintenance_records.csv** Dataset that holds the maintenance records.
10) **Loggers_file.py** This file creates loggers for both data quality checks and application.
11) **fleet_maintenance_MERGED** This file holds the merged and transformed data from both data sources, it is set to be re-written.

## Authors

* Rasim Benkli

## License

Copyright (c) 2024-2025 Scania Academy.
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

