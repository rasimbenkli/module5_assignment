import pandas as pd
import re
from Loggers_file import data_quality_logger


# Counting duplicate rows in the DataFrame
def count_duplicate_rows(dataframe):
    """
    Counts the number of duplicate rows in a given DataFrame and logs the count if duplicate records are found.

    Parameters:
    - dataframe (DataFrame): The input DataFrame to check for duplicate rows.

    Return Value:
    int: The count of duplicate rows in the DataFrame.

    Side Effects:
    If duplicate rows are found, the function logs a message indicating the number of duplicate records found along
    with the name of the dataset (either "Maintenance data" or "fleet data") to the data_quality_logger.

    Assumptions:
    - The DataFrame may have a column named 'maintenance_date'. If present, the DataFrame is assumed to be maintenance data.
    - If 'maintenance_date' column is not present, the DataFrame is assumed to be fleet data.
    - The function utilizes the data_quality_logger object for logging, which should be initialized elsewhere in the code.
    """

    if 'maintenance_date' in dataframe.columns:
        dataset_name = "Maintenance data"
    else:
        dataset_name = "Fleet data"

    duplicates_count = dataframe.duplicated().sum()
    if duplicates_count > 0:
        data_quality_logger.info(f'Duplicate records found for {dataset_name} dataset: {duplicates_count}\n')
    return duplicates_count


# Counting missing values per column in the DataFrame
def count_missing_values_per_column(dataframe):
    """
    Counts the number of missing values in each column of a given DataFrame and logs the counts if missing values are found.

    Parameters:
    - dataframe (DataFrame): The input DataFrame to check for missing values.

    Return Value:
    pandas.Series: A Series containing the count of missing values for each column with missing values, indexed by column names.

    Side Effects:
    If missing values are found in any column, the function logs a message indicating the presence of missing data in the dataset
    along with the count of missing values for each column to the data_quality_logger.

    Assumptions:
    - The DataFrame may have a column named 'maintenance_date'. If present, the DataFrame is assumed to be maintenance data.
    - If 'maintenance_date' column is not present, the DataFrame is assumed to be fleet data.
    - The function utilizes the data_quality_logger object for logging, which should be initialized elsewhere in the code.
    """

    if 'maintenance_date' in dataframe.columns:
        dataset_name = "Maintenance dataset"
    else:
        dataset_name = "Fleet dataset"
    
    missing_values = dataframe.isna().sum()
    filtered_missing_values = missing_values[missing_values > 0]
    
    if not filtered_missing_values.empty:
        data_quality_logger.info(f'Missing data in {dataset_name}\n')
        for index, value in filtered_missing_values.items():
            data_quality_logger.info(f'{index}: {value} missing values\n')

    return filtered_missing_values
   
         
# Find unique truck IDs missing from fleet dataset
def find_unique_truck_ids(df_maintenance, df_fleet):
    """
    Identifies unique truck IDs present in the maintenance dataset but not in the fleet dataset and logs them.

    Parameters:
    - df_maintenance (DataFrame): The DataFrame containing maintenance data.
    - df_fleet (DataFrame): The DataFrame containing fleet data.

    Return Value:
    set: A set containing unique truck IDs present in the maintenance dataset but not in the fleet dataset.

    Side Effects:
    This function logs the unique truck IDs missing from the fleet dataset to the data_quality_logger.

    Assumptions:
    - Both df_maintenance and df_fleet should contain a column named 'truck_id' which represents the unique identifier for each truck.
    - The function utilizes the data_quality_logger object for logging, which should be initialized elsewhere in the code.
    """

    unique_truck_ids = set(df_maintenance['truck_id']) - set(df_fleet['truck_id'])
    data_quality_logger.info(f'Truck IDs missing from the fleet dataset: {unique_truck_ids}\n')
    return unique_truck_ids


# Email validity checker
def check_email_validity(email):
    """
    Checks the validity of an email address based on a regular expression pattern.

    Parameters:
    - email (str): The email address to be checked for validity.

    Return Value:
    bool: True if the email address is valid according to the regex pattern, False otherwise.

    Side Effects:
    None

    Assumptions:
    - The input 'email' parameter is expected to be a string representing an email address.
    """

    regex = r'^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w+$'
    # Check if the email is empty or null
    if pd.isna(email) or email == "":
        return False

    # Check if the email matches the format
    if re.match(regex, email):
        return True
    else:
        return False