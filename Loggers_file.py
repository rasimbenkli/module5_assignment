import logging

# LOGGERS

## Data Quality
data_quality_logger = logging.getLogger('data_quality_logger') # Creating a logger 
data_quality_logger.setLevel(logging.DEBUG) # Setting logging level to DEBUG 
fh_quality = logging.FileHandler('data_quality.log', mode='w') # Creating a file handler 
fh_quality.setLevel(logging.DEBUG) # Setting logging level to DEBUG
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s') # Defining log message format for both data quality logs and application logs
fh_quality.setFormatter(formatter) # Applying the format to the file handler
data_quality_logger.addHandler(fh_quality) # Adding file handlers to the logger

## Application
application_logger = logging.getLogger('application_logger')
application_logger.setLevel(logging.DEBUG)
fh_application = logging.FileHandler('application.log', mode='w')
fh_application.setLevel(logging.DEBUG)
fh_application.setFormatter(formatter)
application_logger.addHandler(fh_application)
