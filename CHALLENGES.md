# My Reflection on the assignment

I loved it!!! 
It felt like a real challenge that we may face when we start working as data engineers. 

# What I didn't know properly before but now learned
- Creating a pipeline
- Creating a source file
- Logging


## Issues I had 

The main issue I had in the beginning was not knowing the logging  and pipeline creation. I got a lot of help from online sources and chatgpt while trying to hold a grasp on those.

I had some issues with importing libraries in between the files (circular import), I imported pandas and re only in one file and other files were taking it from it however it didnt work as I thought it would and I received an error called 'Circular Import' I resolved it by importing the libraries in each file separately.

I solved several small bugs using the help of logging concept.

## My approach on logging

I applied it on the quality check functions as expected but I also applied it for each function call in pipeline. I understand that it is just a simple function call and it may not be necessary to do it however I wanted to have an eye on every step in the pipeline thinking that I may need to create more complex pipelines in the future so I wanted to make it a habbit to myself.



## Overall
I found it very valuable. It made feel more ready for the actual works. Now I have a wider undestanding of how a pipeline should work. 
